package com.nayan.exercise;

public class Product {
    private int prod_id;
    private String prod_name;

    public Product() {}

    public Product(String prod_name) {
        this.prod_name = prod_name;
    }

    public int getProd_id() {
        return prod_id;
    }

    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    public String getProd_name() {
        return prod_name;
    }

    public void setProd_name(String prod_name) {
        this.prod_name = prod_name;
    }

    @Override
    public String toString() {
        return "Product{" + "prod_id=" + prod_id + ", prod_name='" + prod_name + '\'' + '}' + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (prod_id != product.prod_id) return false;
        return prod_name.equals(product.prod_name);
    }

    @Override
    public int hashCode() {
        return prod_id;
    }
}
