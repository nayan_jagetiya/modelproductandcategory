package com.nayan.exercise;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductCategoryDao {
    private Connection con;
    private ProductDao productDao=new ProductDao();
    private CategoryDao categoryDao=new CategoryDao();

    public ProductCategoryDao() throws SQLException, ClassNotFoundException {
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String usrname="Nayan";
        String psswd="root";

        Class.forName("oracle.jdbc.driver.OracleDriver");
        con= DriverManager.getConnection(url,usrname,psswd);
    }
    public static final String INSERT= "insert into ProductCategory values(pc_id.nextval,?,?)";
    public static final String RETRIEVE_ID="select pc_id from ProductCategory where prod_id=? AND cat_id=?";
    public static final String RETRIEVE_PROD_CAT="select prod_id,cat_id from ProductCategory where pc_id=?";
    public static final String RETRIVE_ALL="select pc_id,prod_id,cat_id from ProductCategory order by pc_id";
    public static final String DELETE="delete from ProductCategory where pc_id=?";
    public static final String UPDATE_PRODUCT="update ProductCategory set prod_id=? where pc_id=? ";
    public static final String UPDATE_CATEGORY="update ProductCategory set cat_id=?; where pc_id=?";

    public int add(Product product, List<Category> categoryList) throws SQLException {
        int status=0;

        for(Category c:categoryList){
            if(add(product,c))
                status++;
        }
        return status;
    }

    public boolean add(ProductCategory pc) throws SQLException {
        if(!isProductCategoryExists(pc.getProd_id(),pc.getCat_id())) {
            PreparedStatement ps = con.prepareStatement(INSERT);
            ps.setInt(1, pc.getProd_id());
            ps.setInt(2, pc.getCat_id());
            return ps.executeUpdate() > 0;
        }
        return false;
    }

    public boolean add(Product p,Category c) throws SQLException {
        return add(new ProductCategory(productDao.getId(p.getProd_name()),categoryDao.getId(c.getCat_name())));
    }

    public List<ProductCategory> retriveAll() throws SQLException {
        List<ProductCategory> productCategoryList=new ArrayList<>();
        PreparedStatement ps=con.prepareStatement(RETRIVE_ALL);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            ProductCategory pc=new ProductCategory();
            pc.setPc_id(rs.getInt(1));
            pc.setProd_id(rs.getInt(2));
            pc.setCat_id(rs.getInt(3));
            productCategoryList.add(pc);
        }
        return productCategoryList;
    }

    public int delete(int pc_id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE);
        ps.setInt(1,pc_id);
        return ps.executeUpdate();
    }
    public int delete(ProductCategory pc) throws SQLException {
        return delete(getId(pc.getProd_id(),pc.getCat_id()));
    }
    public int delete(Product p,Category c) throws SQLException {
       return delete(getId(p,c));
    }

    public int updateProduct(int pc_id,int prod_id) throws SQLException {
        int cat_id;
        ResultSet rs=getProdCatId(pc_id);
        if(rs.next()) {
            cat_id = rs.getInt(2);

            if (!isProductCategoryExists(prod_id, cat_id)) {
                PreparedStatement ps = con.prepareStatement(UPDATE_PRODUCT);
                ps.setInt(1, prod_id);
                ps.setInt(2, pc_id);
                return ps.executeUpdate();
            }
        }
        return 0;
    }
    public int updateProduct(int pc_id,Product p) throws SQLException {
        return updateProduct(pc_id,productDao.getId(p.getProd_name()));
    }
    public int updateProduct(ProductCategory pc,int prod_id) throws SQLException {
        return updateProduct(getId(pc),prod_id);
    }
    public int updateProduct(ProductCategory pc,Product p) throws SQLException {
        return updateProduct(getId(pc.getProd_id(),pc.getCat_id()), productDao.getId(p.getProd_name()));
    }


    public int updateCategory(int pc_id,int cat_id) throws SQLException {
        int prod_id;
        ResultSet rs=getProdCatId(pc_id);
        if(rs.next()) {
            prod_id = rs.getInt(1);
            if (!isProductCategoryExists(prod_id,cat_id)) {
                PreparedStatement ps = con.prepareStatement(UPDATE_CATEGORY);
                ps.setInt(1, cat_id);
                ps.setInt(2, pc_id);
                return ps.executeUpdate();
            }
        }
        return 0;
    }
    public int updateCategory(int pc_id,Category c) throws SQLException {
        return updateCategory(pc_id,categoryDao.getId(c.getCat_name()));
    }
    public int updateCategory(ProductCategory pc,int cat_id) throws SQLException {
        return updateCategory(getId(pc),cat_id);
    }
    public int updateCategory(ProductCategory pc,Category c) throws SQLException {
        return updateCategory(getId(pc.getProd_id(),pc.getCat_id()),categoryDao.getId(c.getCat_name()));
    }

    public boolean isProductCategoryExists(int prod_id,int cat_id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_ID);
        ps.setInt(1,prod_id);
        ps.setInt(2,cat_id);
        ResultSet rs=ps.executeQuery();
        return rs.next();
    }
    public int getId(Product p, Category c) throws SQLException {
        return getId(productDao.getId(p.getProd_name()),categoryDao.getId(c.getCat_name()));
    }
    public int getId(ProductCategory pc) throws SQLException {
        return getId(pc.getProd_id(),pc.getCat_id());
    }
    public int getId(int prod_id, int cat_id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_ID);
        ps.setInt(1,prod_id);
        ps.setInt(2,cat_id);
        ResultSet rs=ps.executeQuery();
        if(rs.next())
            return rs.getInt(1);
        else{
            add(new ProductCategory(prod_id,cat_id));
            return getId(prod_id,cat_id);
        }
    }

    public ResultSet getProdCatId(int pc_id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_PROD_CAT);
        ps.setInt(1,pc_id);
        return ps.executeQuery();
    }

    public void closeCon() throws SQLException {
        productDao.closeCon();
        categoryDao.closeCon();
        con.close();
    }
}
