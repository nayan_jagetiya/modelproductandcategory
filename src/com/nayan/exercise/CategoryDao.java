package com.nayan.exercise;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDao {

    private Connection con;

    public CategoryDao() throws ClassNotFoundException, SQLException {
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String usrname="Nayan";
        String psswd="root";

        Class.forName("oracle.jdbc.driver.OracleDriver");
        con= DriverManager.getConnection(url,usrname,psswd);
    }

    public static final String INSERT_CATEGORY_QUERY="insert into category values(cat_id.nextval,?)";
    public static final String UPDATE_CATEGORY_QUERY="update category set cat_name=? where cat_id=?";
    public static final String DELETE_CATEGORY_QUERY="delete from category where cat_id=?";
    public static final String RETRIVE_CATEGORY_QUERY="select cat_id,cat_name from category order by cat_id";
    public static final String RETRIVE_CATEGORY_ID_QUERY="select cat_id from category where cat_name=?";

    public int insert(Category c) throws SQLException {
        if(!isCategoryExist(c)) {
            PreparedStatement ps = con.prepareStatement(INSERT_CATEGORY_QUERY);
            ps.setString(1, c.getCat_name());
            return ps.executeUpdate();
        }
        return 0;
    }

    public int delete(int cat_id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE_CATEGORY_QUERY);
        ps.setInt(1,cat_id);
        return ps.executeUpdate();
    }
    public int delete(Category c) throws SQLException {
       return delete(getId(c.getCat_name()));
    }
    public int update(int id,String new_cat_name) throws SQLException {
        if(!isCategoryExist(new Category(new_cat_name))) {
            PreparedStatement ps = con.prepareStatement(UPDATE_CATEGORY_QUERY);
            ps.setString(1, new_cat_name);
            ps.setInt(2, id);
            return ps.executeUpdate();
        }
        return 0;
    }
    public int update(Category c,String new_cat_name) throws SQLException {
        return update(getId(c.getCat_name()),new_cat_name);
    }

    public List<Category> retriveAll() throws SQLException {
        List<Category> categoryList=new ArrayList<>();
        PreparedStatement ps=con.prepareStatement(RETRIVE_CATEGORY_QUERY);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            Category c=new Category();
            c.setCat_name(rs.getString(2));
            c.setCat_id(rs.getInt(1));
            categoryList.add(c);
        }
        return categoryList;
    }


    public int getId(String cat_name) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIVE_CATEGORY_ID_QUERY);
        ps.setString(1,cat_name);
        ResultSet rs=ps.executeQuery();
        if(rs.next())
            return rs.getInt(1);
        else
            insert(new Category(cat_name));
            return getId(cat_name);

    }

    public boolean isCategoryExist(Category c) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIVE_CATEGORY_ID_QUERY);
        ps.setString(1,c.getCat_name());
        return ps.executeQuery().next();
    }
    public void closeCon() throws SQLException {con.close();}

}
