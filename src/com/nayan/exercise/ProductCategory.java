package com.nayan.exercise;

public class ProductCategory {
    private int pc_id;
    private int prod_id;
    private int cat_id;

    public ProductCategory() {
    }

    public ProductCategory(int prod_id, int cat_id) {
        this.prod_id = prod_id;
        this.cat_id = cat_id;
    }

    public int getPc_id() {
        return pc_id;
    }

    public void setPc_id(int pc_id) {
        this.pc_id = pc_id;
    }

    public int getProd_id() {
        return prod_id;
    }

    public void setProd_id(int prod_id) {
        this.prod_id = prod_id;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    @Override
    public String toString() {
        return "ProductCategory{" +"pc_id=" + pc_id +", prod_id=" + prod_id +", cat_id=" + cat_id +'}'+"\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductCategory that = (ProductCategory) o;

        if (pc_id != that.pc_id) return false;
        if (prod_id != that.prod_id) return false;
        return cat_id == that.cat_id;
    }

    @Override
    public int hashCode() {
        return pc_id;
    }
}
