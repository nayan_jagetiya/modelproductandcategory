package com.nayan.exercise;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        ProductDao productDao;
        CategoryDao categoryDao;
        ProductCategoryDao pcDao;

//        Testing Product Table
//        ----------------------------------------------------------------------------------
//
////        List<Product> products=new ArrayList(){{
////            add(new Product("mi mix 2"));
////            add(new Product("realme7"));
////            add(new Product("luminuous sanganeri"));
////            add(new Product("realme7"));
////            add(new Product("boat rockerz 215"));
////        }};

        try {
            productDao = new ProductDao();
//            for(Product p:products)
//                System.out.println(productDao.insert(p));

//            productDao.update(new Product("boat rockerz 215"),"luminuous sanganeri");
//            productDao.update(new Product("mi mix 2"),"MiMix2");
//            productDao.update(3,"BoatRockerz 215");

//            productDao.delete(new Product("realme7"));
//            productDao.delete(2);
            System.out.println(productDao.retriveAll());
            productDao.closeCon();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



//        Testing Category Table
//        ------------------------------------------

////        List<Category> categories=new ArrayList(){{
////            add(new Category("electronics"));
////            add(new Category("toys"));
////            add(new Category("bags"));
////            add(new Category("toys"));
////            add(new Category("furniture"));
////       }};
//
        try {
            categoryDao = new CategoryDao();

//            for(Category c:categories)
//                System.out.println(categoryDao.insert(c));

//            categoryDao.update(new Category("toys"),"bags");
//            categoryDao.update(new Category("toys"),"TOYS STORE");
//            categoryDao.update(3,"Furniture");

//            System.out.println(categoryDao.delete(new Category("bags")));
//            categoryDao.delete(0);

            System.out.println(categoryDao.retriveAll());
            categoryDao.closeCon();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }



//      Testing for ProductCategory table
//      -------------------------------------

        ProductCategory pc=new ProductCategory(0,3);
        try {
            pcDao=new ProductCategoryDao();
//            Product p=new Product("Asus ROG");
//            List<Category> categories=new ArrayList(){{
//                add(new Category("Electronics"));
//                add(new Category("Phone"));
//                add(new Category("TOYS STORE"));
//                add(new Category("GamingPhone"));
//            }};

//            System.out.println(pc));
//            System.out.println(pcDao.add(p,categories));
//            System.out.println(pcDao.add(new Product("realme7"),new Category("Phone")));
//            System.out.println(pcDao.add(new Product("Asus ROG"),new Category("Phone")));

//            pcDao.delete(new ProductCategory(1,4));
//            pcDao.delete(new Product("realme7"),new Category("Phone"));
//            pcDao.delete(22);

//        pcDao.updateProduct(new ProductCategory(2,2),5);
//        pcDao.updateCategory(22,new Category("TOY"))

            System.out.println(pcDao.retriveAll());
            pcDao.closeCon();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }
}
