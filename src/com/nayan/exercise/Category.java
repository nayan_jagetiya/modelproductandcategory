package com.nayan.exercise;

public class Category {
    private int cat_id;
    private String cat_name;

    public Category(){}
    public Category(String cat_name) {
        this.cat_name = cat_name;
    }

    public int getCat_id() {
        return cat_id;
    }

    public void setCat_id(int cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    @Override
    public String toString() {
        return "Category{" +"cat_id=" + cat_id +", cat_name='" + cat_name + '\'' +'}'+ "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Category category = (Category) o;

        if (cat_id != category.cat_id) return false;
        return cat_name.equals(category.cat_name);
    }

    @Override
    public int hashCode() {
        return cat_id;
    }
}
