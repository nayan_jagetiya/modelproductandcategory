package com.nayan.exercise;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductDao {

    private Connection con;

    public ProductDao() throws ClassNotFoundException, SQLException {
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String usrname="Nayan";
        String psswd="root";

        Class.forName("oracle.jdbc.driver.OracleDriver");
        con= DriverManager.getConnection(url,usrname,psswd);
    }

    public static final String INSERT_PRODUCT_QUERY="insert into product values(prod_id.nextval,?)";
    public static final String UPDATE_PRODUCT_QUERY="update product set prod_name=? where prod_id=?";
    public static final String DELETE_PRODUCT_QUERY="delete from product where prod_id=?";
    public static final String RETRIVE_PRODUCT_QUERY="select prod_id,prod_name from product order by prod_id";
    public static final String RETRIVE_PRODUCT_ID_QUERY="select prod_id from product where prod_name=?";

    public int insert(Product p) throws SQLException {
        if(!isProductExist(p)) {
            PreparedStatement ps = con.prepareStatement(INSERT_PRODUCT_QUERY);
            ps.setString(1, p.getProd_name());
            return ps.executeUpdate();
        }
        return 0;
    }

    public int delete(int id) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE_PRODUCT_QUERY);
        ps.setInt(1,id);
        return ps.executeUpdate();
    }
    public int delete(Product p) throws SQLException {
        return delete(getId(p.getProd_name()));
    }

    public int update(int id,String new_prod_name) throws SQLException, ClassNotFoundException {
        if(!isProductExist(new Product(new_prod_name))) {
            PreparedStatement ps = con.prepareStatement(UPDATE_PRODUCT_QUERY);
            ps.setString(1, new_prod_name);
            ps.setInt(2,id);
            return ps.executeUpdate();
        }
        return 0;
    }
    public int update(Product p,String new_prod_name) throws SQLException, ClassNotFoundException {
        return update(getId(p.getProd_name()),new_prod_name);
    }

    public List<Product> retriveAll() throws SQLException {
        List<Product> productList=new ArrayList<>();
        PreparedStatement ps=con.prepareStatement(RETRIVE_PRODUCT_QUERY);
        ResultSet rs=ps.executeQuery();
        while(rs.next()){
            Product p=new Product();
            p.setProd_name(rs.getString(2));
            p.setProd_id(rs.getInt(1));
            productList.add(p);
        }
        return productList;
    }


    public int getId(String prod_name) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIVE_PRODUCT_ID_QUERY);
        ps.setString(1,prod_name);
        ResultSet rs=ps.executeQuery();
        if(rs.next())
            return rs.getInt(1);
        else
            insert(new Product(prod_name));
        return getId(prod_name);

    }

    public boolean isProductExist(Product p) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIVE_PRODUCT_ID_QUERY);
        ps.setString(1,p.getProd_name());
        return ps.executeQuery().next();
    }
    public void closeCon() throws SQLException {con.close();}

}

