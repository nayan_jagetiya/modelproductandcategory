package com.nayan.reexercise.Main;

import com.nayan.reexercise.model.Category;
import com.nayan.reexercise.model.Pagination;
import com.nayan.reexercise.model.Product;
import com.nayan.reexercise.model.Sort;
import com.nayan.reexercise.service.CategoryService;
import com.nayan.reexercise.service.ProductService;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) throws SQLException, ClassNotFoundException, ParseException {

//        Testing ProductService
//        -----------------------

        ProductService productService=new ProductService();
        SimpleDateFormat sdf=new SimpleDateFormat("dd/mm/yyyy");

//        Product product =new Product("AsusRog",42000,new Date(),new ArrayList(){{
//            add(new Category("Electronics"));
//            add(new Category("Mobile"));
//            add(new Category("GamingPhone"));
//        }});
//        Product product1 =new Product("realme7",17000,new Date(2021,06,23),new ArrayList(){{
//            add(new Category("Electronics"));
//            add(new Category("Mobile"));
//            add(new Category("SmartPhone"));
//        }});

//        productService.addProduct(product);
//        productService.addProduct(product1);

//        productService.updateProduct( new Product("Realme 7",16999,new Date(System.currentTimeMillis())));

//        productService.deleteProduct("Realme 7");


//        java.util.Date d=sdf.parse("12/01/2019");
//        productService.updateProduct("Asus Rog",42000,new Date(d.getTime()));

//        productService.addProduct(new Product("Study Table",new Date(sdf.parse("9/7/2018").getTime()),new ArrayList(){{
//            add(new Category("Education"));
//            add(new Category("Furniture"));
//        }}));


//
        System.out.println(productService.getProducts(new Pagination(),new Sort(Sort.SortCriteria.PROD_NAME, Sort.SortOrder.DESC),""));
//


//        Testing Category Service
//        ---------------------------
//        CategoryService categoryService=new CategoryService();
//        List<String> products=categoryService.getProducts(new ArrayList(){{
//            add("Electronics");
//            add("GamingPhone");
//        }});
//        System.out.println(products);


//        productService.closeConnection();
//        categoryService.closeConnection();

    }
}
