package com.nayan.reexercise.helperclass;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionProvider {
    public static Connection getConnection() throws ClassNotFoundException, SQLException {
        String url="jdbc:oracle:thin:@localhost:1521:XE";
        String usrname="Nayan";
        String psswd="root";

        Class.forName("oracle.jdbc.driver.OracleDriver");
        return DriverManager.getConnection(url,usrname,psswd);
    }
//    public static Connection getConnection(String url,String username,String password) throws ClassNotFoundException, SQLException {
//        Class.forName("oracle.jdbc.driver.OracleDriver");
//        return DriverManager.getConnection(url,username,password);
//    }
}
