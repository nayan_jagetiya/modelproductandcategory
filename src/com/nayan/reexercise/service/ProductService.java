package com.nayan.reexercise.service;

import com.nayan.reexercise.dao.CategoryDao;
import com.nayan.reexercise.dao.ProductCategoryDao;
import com.nayan.reexercise.dao.ProductDao;
import com.nayan.reexercise.model.Category;
import com.nayan.reexercise.model.Pagination;
import com.nayan.reexercise.model.Product;
import com.nayan.reexercise.model.Sort;

import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductService {
    private ProductDao productDao;
    private CategoryDao categoryDao;
    private ProductCategoryDao productCategoryDao;

    public ProductService() throws SQLException, ClassNotFoundException {
        productDao = new ProductDao();
        categoryDao = new CategoryDao();
        productCategoryDao = new ProductCategoryDao();
    }

    public int addProduct(Product product) throws SQLException {
        int productId, categoryId, addStatus = 0;

        productId = productDao.getId(product.getProductName());
        if (productId == -1) {
            productDao.insert(product.getProductName(),product.getPrice(),product.getLaunchDate());
            productId = productDao.getId(product.getProductName());
        }
        for (Category category : product.getCategories()) {
            categoryId = categoryDao.getId(category.getCategoryName());
            if (categoryId == -1) {
                categoryDao.insert(category.getCategoryName());
                categoryId = categoryDao.getId(category.getCategoryName());
            }

            if (productCategoryDao.getId(productId, categoryId) == -1) {
                productCategoryDao.insert(productId, categoryId);
                addStatus++;
            }
        }
        return addStatus;
    }

    public int updateProduct(Product product) throws SQLException {
        int updateStatus = 0;
        Product originalProduct=productDao.getProductById(product.getProductId());
        if(!originalProduct.equals(product)){
            return productDao.update(originalProduct);
//            if(!product.getProductName().equals(originalProduct.getProductName())){
//                updateStatus=productDao.update(product.getProductId(),product.getProductName());
//            }
//            if(product.getLaunchDate().equals(originalProduct.getLaunchDate())){
//                updateStatus+=productDao.update(product.getProductId(),product.getLaunchDate());
//            }
//            if(product.getPrice()==originalProduct.getPrice()){
//                updateStatus+=productDao.update(product.getProductId(),product.getPrice());
//            }
        }
        return updateStatus;
    }

    public boolean deleteProduct(String productName) throws SQLException {
        boolean deleteStatus = false;
        int id=productDao.getId(productName);
        if (id != -1) {
            productCategoryDao.deleteProduct(id);
            deleteStatus = productDao.delete(id);
        }
        return deleteStatus;
    }

    public List<Product> getProducts(Pagination page,Sort sort,String searchString) throws SQLException {
        page.setOffset(page.getPageCount() * page.getLimit());
        return productDao.get(page,sort,searchString);
    }

    public void closeConnection() throws SQLException {
        productDao.closeConnection();
        categoryDao.closeConnection();
        productCategoryDao.closeConnection();
    }

}

