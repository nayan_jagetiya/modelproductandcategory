package com.nayan.reexercise.service;

import com.nayan.reexercise.dao.CategoryDao;
import com.nayan.reexercise.dao.ProductCategoryDao;
import com.nayan.reexercise.dao.ProductDao;
import com.nayan.reexercise.model.Pagination;
import com.nayan.reexercise.model.Product;

import java.sql.SQLException;
import java.util.*;

public class CategoryService {
    private ProductDao productDao;
    private CategoryDao categoryDao;
    private ProductCategoryDao productCategoryDao;

    public CategoryService() throws SQLException, ClassNotFoundException {
        productDao=new ProductDao();
        categoryDao=new CategoryDao();
        productCategoryDao=new ProductCategoryDao();
    }

    public List<String> getProducts(List<String> categoryNames, Pagination page) throws SQLException {
        Set<String> products=new LinkedHashSet<>();
        page.setOffset((page.getPageCount()-1) * page.getLimit());
        for(String categoryName:categoryNames){
            int categoryId=categoryDao.getId(categoryName);
            products.addAll(categoryDao.getProducts(categoryId,page));
        }
        return new ArrayList<>(products);
    }

    public void closeConnection() throws SQLException {
        productDao.closeConnection();
        categoryDao.closeConnection();
        productCategoryDao.closeConnection();
    }

}