package com.nayan.reexercise.model;

public class Sort {
//    private static final SortCriteria DEFAULT_SORT_CRITERIA=SortCriteria.LAUNCH_DATE;
//    private static final SortOrder DEFAULT_SORT_ORDER=SortOrder.DESC;
    SortCriteria sortCriteria;
    SortOrder sortOrder;
    public enum SortOrder{ASC,DESC}
    public enum SortCriteria{LAUNCH_DATE,PROD_NAME,PRICE,PROD_ID}

    public Sort() {
        this.sortCriteria=SortCriteria.LAUNCH_DATE;
        this.sortOrder=SortOrder.DESC;
    }

    public Sort(SortCriteria sortCriteria, SortOrder sortOrder) {
        this.sortCriteria = sortCriteria;
        this.sortOrder = sortOrder;
    }

    public SortCriteria getSortCriteria() {
        return sortCriteria;
    }

    public void setSortCriteria(SortCriteria sortCriteria) {
        this.sortCriteria = sortCriteria;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
    }
}
