package com.nayan.reexercise.dao;

import com.nayan.reexercise.helperclass.ConnectionProvider;
import com.nayan.reexercise.model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ProductDao {
    private Connection con;

    public ProductDao() throws ClassNotFoundException, SQLException {
        con= ConnectionProvider.getConnection();
    }

    private static final String INSERT="INSERT INTO PRODUCT VALUES(prod_id.nextval,?,?,?)";
    private static final String RETRIEVE_ID ="SELECT prod_id from PRODUCT WHERE prod_name=?";
    private static final String RETRIEVE_PRODUCT_BY_ID="SELECT prod_name,price,launch_date FROM PRODUCT WHERE prod_id=?";
//    private static final String UPDATE="UPDATE PRODUCT SET "+column+" = ? WHERE prod_id=?";
    private static final String UPDATE1="UPDATE PRODUCT SET prod_name=?, price=?, launch_date=? WHERE prod_id=?";
    private static final String DELETE="DELETE FROM PRODUCT WHERE prod_id=?";
    private static final String RETRIEVE_CATEGORIES="SELECT cat_id,cat_name FROM CATEGORY WHERE cat_id IN (SELECT cat_id FROM PRODUCTCATEGORY WHERE prod_id=?)";

    public boolean insert(String productName,double price, Date date) throws SQLException {
        PreparedStatement ps=con.prepareStatement(INSERT);
        ps.setString(1,productName);
        ps.setDouble(2,price);
        ps.setDate(3,new java.sql.Date(date.getTime()));
        return ps.executeUpdate()>0;
    }

    public Product getProductById(int productId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_PRODUCT_BY_ID);
        ps.setInt(1,productId);
        ResultSet rs=ps.executeQuery();
        return new Product(rs.getString("prod_name"),rs.getDouble("price"),rs.getDate("launch_date"));
    }

    public List<Product> get(Pagination pagination, Sort sort, String searchString) throws SQLException {
        List<Product> products=new ArrayList<>();
        String column=sort.getSortCriteria().toString();
        String order= sort.getSortOrder().toString();
        String retrieve="SELECT prod_id,prod_name,price,launch_date from(SELECT ROWNUM RNUM,P.* FROM PRODUCT P WHERE lower(P.prod_name) LIKE lower(?) ORDER BY "+column+" "+order+") WHERE RNUM>? AND RNUM<=?";

        PreparedStatement ps=con.prepareStatement(retrieve);
        ps.setString(1,searchString+"%");
        ps.setInt(2,pagination.getOffset());
        ps.setInt(3,pagination.getOffset()+pagination.getLimit());
        ResultSet rs=ps.executeQuery();
        while (rs.next()){
            Product product=new Product();
            product.setProductId(rs.getInt(1));
            product.setProductName(rs.getString(2));
            product.setPrice(rs.getDouble(3));
            product.setLaunchDate(rs.getDate(4));
//            product.setCategories(getCategories(product.getProductId()));
            products.add(product);
        }
        return products;
    }


    public List<Category> getCategories(int productId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_CATEGORIES);
        ps.setInt(1,productId);
        ResultSet rs=ps.executeQuery();

        List<Category> categories=new ArrayList<>();
        while (rs.next()){
            Category category=new Category();
            category.setCategoryId(rs.getInt(1));
            category.setCategoryName(rs.getString(2));
            categories.add(category);
        }
        return categories;
    }

    public int update(Product product) throws SQLException {
        PreparedStatement ps=con.prepareStatement(UPDATE1);
        ps.setString(1,product.getProductName());
        ps.setDouble(2,product.getPrice());
        ps.setDate(3,new java.sql.Date(product.getLaunchDate().getTime()));
        return ps.executeUpdate();
    }

//    public int update(int id,String newProductName) throws SQLException {
//        column="prod_name";
//        PreparedStatement ps=con.prepareStatement(UPDATE);
//        ps.setString(1,newProductName);
//        ps.setInt(2,id);
//        return ps.executeUpdate();
//    }
//    public int update(int id, Date date) throws SQLException {
//        column="launch_date";
//        PreparedStatement ps=con.prepareStatement(UPDATE);
//        ps.setDate(1, new java.sql.Date(date.getTime()));
//        ps.setInt(2,id);
//        return ps.executeUpdate();
//    }
//    public int update(int id, double price) throws SQLException {
//        column="price";
//        PreparedStatement ps=con.prepareStatement(UPDATE);
//        ps.setDouble(1, price);
//        ps.setInt(2,id);
//        return ps.executeUpdate();
//    }

    public boolean delete(int productId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE);
        ps.setInt(1,productId);
        return ps.executeUpdate()>0;
    }

    public int getId(String productName) throws SQLException {
        PreparedStatement ps = con.prepareStatement(RETRIEVE_ID);
        ps.setString(1, productName);
        ResultSet rs=ps.executeQuery();
        if (rs.next())
            return rs.getInt(1);
        return -1;
    }

//    public boolean productExists(Product product) throws SQLException {
//        ps=con.prepareStatement(RETRIEVE_ID);
//        ps.setString(1,product.getProductName());
//        return ps.executeQuery().next();
//    }

    public void closeConnection() throws SQLException {
        con.close();
    }

}
