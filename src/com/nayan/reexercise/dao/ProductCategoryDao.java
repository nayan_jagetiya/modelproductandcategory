package com.nayan.reexercise.dao;

import com.nayan.reexercise.helperclass.ConnectionProvider;
import com.nayan.reexercise.model.Category;
import com.nayan.reexercise.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductCategoryDao {
    private Connection con;

    public ProductCategoryDao() throws ClassNotFoundException, SQLException {
        con= ConnectionProvider.getConnection();
    }

    private static final String INSERT="INSERT INTO PRODUCTCATEGORY VALUES(pc_id.nextval,?,?)";
    private static final String RETRIEVE_ID ="SELECT pc_id FROM PRODUCTCATEGORY WHERE prod_id=? AND cat_id=?";
    private static final String DELETE_USING_CATEGORY="DELETE FROM PRODUCTCATEGORY WHERE cat_id=?";
    private static final String DELETE_USING_PRODUCT="DELETE FROM PRODUCTCATEGORY WHERE prod_id=?";

    public boolean insert(int productId,int categoryId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(INSERT);
        ps.setInt(1,productId);
        ps.setInt(2,categoryId);
        return ps.executeUpdate()>0;
    }

    public boolean deleteProduct(int productId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE_USING_PRODUCT);
        ps.setInt(1,productId);
        return ps.executeUpdate()>0;
    }

    public boolean deleteCategory(int categoryId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(DELETE_USING_CATEGORY);
        ps.setInt(1,categoryId);
        return ps.executeUpdate()>0;
    }

    public int getId(int productId,int categoryId) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_ID);
        ps.setInt(1,productId);
        ps.setInt(2,categoryId);
        ResultSet rs= ps.executeQuery();
        if (rs.next())
            rs.getInt(1);
        return -1;
    }

    public void closeConnection() throws SQLException {
        con.close();
    }
}
