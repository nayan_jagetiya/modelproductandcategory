package com.nayan.reexercise.dao;

import com.nayan.reexercise.helperclass.ConnectionProvider;
import com.nayan.reexercise.model.Category;
import com.nayan.reexercise.model.Pagination;
import com.nayan.reexercise.model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CategoryDao {
    private Connection con;

    public CategoryDao() throws ClassNotFoundException, SQLException {
        con= ConnectionProvider.getConnection();
    }

    private static final String INSERT="INSERT INTO CATEGORY VALUES(cat_id.nextval,?)";
    private static final String RETRIEVE="SELECT cat_id,cat_name FROM (SELECT ROWNUM RNUM,C.* FROM CATEGORY) WHERE RNUM>? AND RNUM<=?";
    private static final String RETRIEVE_PRODUCTS="SELECT prod_name FROM (SELECT ROWNUM RNUM,P.* from PRODUCT p WHERE prod_id IN (SELECT prod_id FROM PRODUCTCATEGORY WHERE cat_id=?)) WHERE RNUM>? AND RNUM<=?";
    private static final String RETRIEVE_ID="SELECT cat_id FROM CATEGORY WHERE cat_name=?";

    public boolean insert(String categoryName) throws SQLException {
        PreparedStatement ps=con.prepareStatement(INSERT);
        ps.setString(1,categoryName);
        return ps.executeUpdate()>0;
    }

    public List<String> getProducts(int categoryId, Pagination page) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_PRODUCTS);
        ps.setInt(1,categoryId);
        ps.setInt(2,page.getOffset());
        ps.setInt(3,page.getLimit());
        ResultSet rs=ps.executeQuery();

        List<String> products=new ArrayList<>();
        while(rs.next()){
            products.add(rs.getString(1));
        }
        return products;
    }

    public int getId(String categoryName) throws SQLException {
        PreparedStatement ps=con.prepareStatement(RETRIEVE_ID);
        ps.setString(1,categoryName);
        ResultSet rs=ps.executeQuery();
        if (rs.next())
            return rs.getInt(1);
        return -1;
    }

//    public boolean categoryExists(Category category) throws SQLException {
//        ps=con.prepareStatement(RETRIEVE_ID);
//        ps.setString(1,category.getCategoryName());
//        return ps.executeQuery().next();
//    }

    public void closeConnection() throws SQLException {
        con.close();
    }
}
